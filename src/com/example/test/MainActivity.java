package com.example.test;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	private ViewPager tab;
	private TabPagerAdapter tabAdapter;
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tabAdapter = new TabPagerAdapter(getSupportFragmentManager());
		tab = (ViewPager) findViewById(R.id.pager);
		tab.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				actionBar = getSupportActionBar();
				actionBar.setSelectedNavigationItem(position);
			}
		});

		tab.setAdapter(tabAdapter);
		actionBar = getSupportActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			@Override
			public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				if (arg0.getPosition() == 0)
					arg0.setIcon(R.drawable.notifications_36_inactive);
				else if (arg0.getPosition() == 1)
					arg0.setIcon(R.drawable.addressbookt_36_inactive);
				else if (arg0.getPosition() == 2)
					arg0.setIcon(R.drawable.alert_36_inactive);
			}

			@Override
			public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				tab.setCurrentItem(arg0.getPosition());

				if (arg0.getPosition() == 0) {
					arg0.setIcon(R.drawable.notifications_36);
					actionBar.setTitle("Notifications");
				} else if (arg0.getPosition() == 1) {
					arg0.setIcon(R.drawable.addressbookt_36);
					actionBar.setTitle("Address Book");
				} else if (arg0.getPosition() == 2) {
					arg0.setIcon(R.drawable.alert_36);
					actionBar.setTitle("Instant Messaging");
				}
			}

			@Override
			public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
				// TODO Auto-generated method stub

			}
		};

		actionBar.addTab(actionBar.newTab()
				.setIcon(R.drawable.notifications_36)
				.setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setIcon(R.drawable.addressbookt_36)
				.setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setIcon(R.drawable.alert_36)
				.setTabListener(tabListener));

		actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.WHITE));
		actionBar.setTitle("Instant Messaging");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void replaceFragment() {
		// FragmentManager manager = getSupportFragmentManager();
		// FragmentTransaction transaction = manager.beginTransaction();
		actionBar.setTitle("button1");
		// transaction.commit();

		// Toast.makeText(getBaseContext(), "i will replace",
		// Toast.LENGTH_SHORT).show();
	}

}
