package com.example.test;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Android extends Fragment {
	private View parentView;
	private String state;
	private Button btn1, btn2, btn3;
	
	SharedPreferences shared;
	
	@Override
	public View onCreateView(final LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		parentView = inflater.inflate(R.layout.android_frag, container,false);
		
		shared = PreferenceManager.getDefaultSharedPreferences(getActivity());
		state = shared.getString("androidstate", null);
		
		setupButtons(inflater);
		
		processScreenToShow(inflater);

		
		
		return parentView;
	}
	
	private void processScreenToShow(LayoutInflater inflater) {
		if (state == null || state.equals("1")) {
			loadFirstScreen(inflater);
		} else if (state.equals("2")) {
			loadSecondScreen(inflater);
		} else if (state.equals("3")) {
			loadThirdScreen(inflater);
		}
		
		Toast.makeText(getActivity(), "state is: " + state, Toast.LENGTH_LONG).show();
	}
	
	private void loadThirdScreen(LayoutInflater inflater) {
		LinearLayout contentContainer = (LinearLayout) parentView
				.findViewById(R.id.container_content);
		
		contentContainer.removeAllViews();
		inflater.inflate(R.layout.base3, contentContainer, true);
		
		SharedPreferences.Editor edit = shared.edit();
		edit.putString("androidstate","3" );
		edit.commit();
		
		enableButtons(3);
	}
	
	private void loadSecondScreen(LayoutInflater inflater) {
		LinearLayout contentContainer = (LinearLayout) parentView
				.findViewById(R.id.container_content);
		
		contentContainer.removeAllViews();
		inflater.inflate(R.layout.base2, contentContainer, true);
		
		SharedPreferences.Editor edit = shared.edit();
		edit.putString("androidstate","2" );
		edit.commit();
		
		enableButtons(2);
	}
	
	private void loadFirstScreen(LayoutInflater inflater) {
		LinearLayout contentContainer = (LinearLayout) parentView
				.findViewById(R.id.container_content);
		
		contentContainer.removeAllViews();
		inflater.inflate(R.layout.base1, contentContainer, true);
		
		SharedPreferences.Editor edit = shared.edit();
		edit.putString("androidstate","1" );
		edit.commit();
		
		enableButtons(1);
	}
	
	private void enableButtons(int n) {
		if (n == 1) {
			btn1.setEnabled(false);
			btn2.setEnabled(true);
			btn3.setEnabled(true);
		} else if (n == 2) {
			btn1.setEnabled(true);
			btn2.setEnabled(false);
			btn3.setEnabled(true);
		} else if (n == 3) {
			btn1.setEnabled(true);
			btn2.setEnabled(true);
			btn3.setEnabled(false);
		}
	}

	
	private void setupButtons(final LayoutInflater inflater) {
		btn1 = (Button) parentView.findViewById(R.id.button1);
		btn2 = (Button) parentView.findViewById(R.id.button2);
		btn3 = (Button) parentView.findViewById(R.id.button3);
		
		btn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadThirdScreen(inflater);
			}
		});
		
		btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadSecondScreen(inflater);
			}
		});

		btn1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadFirstScreen(inflater);
			}
		});
	}
}
