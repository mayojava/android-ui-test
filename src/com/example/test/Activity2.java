package com.example.test;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;

public class Activity2 extends ActionBarActivity {
	private ViewPager pager;
	private ActionBar actionBar;
	private TabPagerAdapter tabAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		pager = (ViewPager)findViewById(R.id.pager);
		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				actionBar = getSupportActionBar();
				actionBar.setSelectedNavigationItem(arg0);
			}
			
		});
		
		
		tabAdapter = new TabPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(tabAdapter);
		
		setupTabs();
	}
	
	private void setupTabs() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);
		
		
		Tab tab1 = actionBar.newTab()
				.setIcon(R.drawable.notifications_36)
				.setTag("noticationsfragment")
				.setTabListener(new SupportFragmentTabListener(pager))
				;
		
		actionBar.addTab(tab1);
		actionBar.selectTab(tab1);
		
		Tab tab2 = actionBar.newTab()
				.setIcon(R.drawable.addressbookt_36)
				.setTag("instantmessaging")
				.setTabListener(new SupportFragmentTabListener(pager))
				;
		actionBar.addTab(tab2);
		
		Tab tab3 = actionBar.newTab()
				.setIcon(R.drawable.alert_36)
				.setTag("noticationsfragment")
				.setTabListener(new SupportFragmentTabListener(pager))
				;
		actionBar.addTab(tab3);
		
	}
}
