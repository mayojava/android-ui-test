package com.example.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;

public class SupportFragmentTabListener implements TabListener {
	ViewPager pager;
	
	public SupportFragmentTabListener(ViewPager pager) {
		this.pager = pager;
	}
	
	/*private Fragment mFragment;
	private final FragmentActivity mActivity;
	private final String mTag;
	private final Class<T> mClass;
	private final int mFragmentContainerId;
	private final Bundle mFragmentArgs;
	
	public SupportFragmentTabListener(FragmentActivity activity, String tag, Class<T> claz) {
		this.mActivity = activity;
		this.mTag = tag;
		this.mClass = claz;
		mFragmentArgs = new Bundle();
		mFragmentContainerId = android.R.id.content;
	}
	
	public SupportFragmentTabListener (int fragmentContainerId, FragmentActivity activity, String tag, Class<T> claz) {
		this.mActivity = activity;
		this.mTag = tag;
		this.mClass = claz;
		this.mFragmentContainerId = fragmentContainerId;
		mFragmentArgs =  new Bundle();
	}
	
	public SupportFragmentTabListener(int fragmentContainerId, FragmentActivity activity, String tag, Class<T> claz, Bundle args) {
		this.mActivity = activity;
		this.mTag = tag;
		this.mClass = claz;
		this.mFragmentContainerId = fragmentContainerId;
		this.mFragmentArgs = args;
	}*/

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		pager.setCurrentItem(arg0.getPosition());
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

}
