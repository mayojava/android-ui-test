package com.example.test;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class IOS extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View ios = inflater.inflate(R.layout.ios_frag, container, false);
		((TextView) ios.findViewById(R.id.textView)).setText("iOS");
		return ios;
	}
}
